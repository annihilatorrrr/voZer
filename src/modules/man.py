# Locals
man_not_found = 'Manual not found.'

import os

if not os.path.exists(f'{config.modules_dir}/man/'):
    os.mkdir(f'{config.modules_dir}/man/')

@dp.message_handler(commands='man')
async def get_manual(message: types.Message):
    if not os.path.exists(f'{config.modules_dir}/man/{message.text[5:]}') or '..' in message.text:
        await message.reply(man_not_found)
    if os.path.exists(f'{config.modules_dir}/man/{message.text[5:]}') and '..' not in message.text:
        await message.reply(open(f'{config.modules_dir}/man/{message.text[5:]}').read())

@dp.message_handler(commands='listman')
async def get_manuals_list(message: types.Message):
    man_list = ''
    for man in os.listdir(f'{config.modules_dir}/man/'):
        man_list += f'- {man}\n'
    await message.reply(f'{man_list}\n/man [man_name]')
