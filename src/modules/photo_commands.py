import PIL, PIL.Image, PIL.ImageDraw, PIL.ImageFont
from wand.image import Image
from wand.display import display
from wand.font import Font
from local import *
import time
import os

# Locals 
jpeg_error = 'The picture quality should be an integer number from 1 to 100.'

# Configs
jpeg_id = time.time()
dem_id = time.time()
dope_id = time.time()

async def create(message, urls: int, quality: int) -> list[bytes]:
        result = []
        img = Image(filename=urls).convert('RGBA')
        img.transform(resize='500x500>')
        img.transform(resize='500x500<')
        img.format = 'jpeg'
        img.compression_quality = quality
        img.save(filename=f'Temp/result_{message.chat.id}_{jpeg_id}.jpg')

def add_text(message, text):
    template = PIL.Image.open(f'{config.modules_dir}Dem/template.jpg')
    meme = PIL.Image.open(f'{config.modules_dir}Dem/Threads/thread_{message.chat.id}_{dem_id}.jpg').convert('RGBA')
    width = 610
    height = 519
    resized_meme = meme.resize((width, height), PIL.Image.ANTIALIAS)
    text_position = (0, 0)
    text_color = (266,0,0)
    strip_width, strip_height = 700, 1300
    def findLen(text_len):
        counter = 0    
        for i in text_len:
            counter += 1
        return counter
    font_width = 60   
    if findLen(text) >= 25:
        font_width = 50
    background = PIL.Image.new('RGB', (strip_width, strip_height)) # Creating the black strip
    draw = PIL.ImageDraw.Draw(template)
    if '\n' in text:  
        split_offers = text.split('\n')
        for i in range(2):
            if i == 1:
                strip_height += 110
                font_width -= 20
            font = PIL.ImageFont.truetype(config.modules_dir + 'Dem/font.ttf', font_width) 
            text_width, text_height = draw.textsize(split_offers[i], font)

            position = ((strip_width-text_width)/2,(strip_height-text_height)/2)
            draw.text(position, split_offers[i], font=font)
    else:
        font = PIL.ImageFont.truetype(config.modules_dir + 'Dem/font.ttf', font_width)
        text_width, text_height = draw.textsize(text, font)
        strip_height = 1330
        position = ((strip_width-text_width)/2,(strip_height-text_height)/2)
        draw.text(position, text, font=font)
    template.paste(resized_meme, (54, 32),  resized_meme)
    template.save(f'{config.modules_dir}Dem/Threads/thread_{message.chat.id}_{dem_id}_ok.jpg')


@dp.message_handler(content_types=['photo'])
async def photo(message):
    if message.caption.startswith('/dem'):
        await message.photo[-1].download(destination_file=f'{config.modules_dir}Dem/Threads/thread_{message.chat.id}_{dem_id}.jpg')
        text = message.caption.replace('/dem', '')
        add_text(message, text)
        await bot.send_photo(message.chat.id, photo=open(f'{config.modules_dir}Dem/Threads/thread_{message.chat.id}_{dem_id}_ok.jpg', 'rb'))
        os.remove(f'{config.modules_dir}Dem/Threads/thread_{message.chat.id}_{dem_id}.jpg')
        os.remove(f'{config.modules_dir}Dem/Threads/thread_{message.chat.id}_{dem_id}_ok.jpg')
    if message.caption == '/dopeaphoto':
        await message.photo[-1].download(destination_file=f'Temp/img_{message.chat.id}_{dope_id}.jpg')
        user_img = PIL.Image.open(f'Temp/img_{message.chat.id}_{dope_id}.jpg').convert("RGBA")
        (width, height) = user_img.size
        image = Image(filename=f'Temp/img_{message.chat.id}_{dope_id}.jpg')
        with image.clone() as liquid:
            liquid.liquid_rescale(
                int(user_img.size[0]*0.5), int(user_img.size[1]*0.5))
            liquid.save(filename=f'Temp/result_{message.chat.id}_{dope_id}.jpg')
            liquid.size
        user_img = PIL.Image.open(
            f'Temp/result_{message.chat.id}_{dope_id}.jpg').resize((width, height))
        user_img.save(f'Temp/result_{message.chat.id}_{dope_id}.jpg')
        await bot.send_photo(message.chat.id, photo=open(f'Temp/result_{message.chat.id}_{dope_id}.jpg', 'rb'))
        os.remove(f'Temp/result_{message.chat.id}_{dope_id}.jpg')
        os.remove(f'Temp/img_{message.chat.id}_{dope_id}.jpg')
    if message.caption.startswith('/jpeg'):
        await message.photo[-1].download(destination_file=f'Temp/img_{message.chat.id}_{jpeg_id}.jpg')
        quality = int(message.caption.replace('/jpeg ', ''))
        try:
            if not 1 <= quality <= 100:
                raise ValueError
        except ValueError:
            await message.reply(jpeg_error)
            return
        except TypeError:
            quality = 93
        quality = 101 - quality 
        await create(message, f'Temp/img_{message.chat.id}_{jpeg_id}.jpg', quality)  
        await bot.send_photo(message.chat.id, photo=open(f'Temp/result_{message.chat.id}_{jpeg_id}.jpg', 'rb'))
        os.remove(f'Temp/img_{message.chat.id}_{jpeg_id}.jpg')
        os.remove(f'Temp/result_{message.chat.id}_{jpeg_id}.jpg')  

