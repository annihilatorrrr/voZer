# Locals
test_message = "<b>Test message!</b> If you can see this, it's OK."
broadcasting = '<i>Broadcasting...</i>'
broadcasting_test_message = '<i>Broadcasting test message...</i>'
broadcast_warn = '<b>⚠ Notification!</b>\n'

@dp.message_handler(commands='broadcast')
async def broadcast(message: types.Message):
    if message.from_user.id in config.admins and message.text != '/broadcast' and message.text != '/broadcast test':
        await message.answer(broadcasting, parse_mode='HTML')
        chats_list = os.listdir('Bases/')
        chats_list.remove('botbase.txt')
        for chat in chats_list:
            try:
                await bot.send_message(int(chat[:-4]), f'{broadcast_warn}{message.text[message.entities[0].length:]}', parse_mode=types.ParseMode.HTML)
            except:
                pass
    if message.from_user.id in config.admins and message.text == '/broadcast test':
        await message.answer(broadcasting_test_message, parse_mode='HTML')
        chats_list = os.listdir('Bases/')
        chats_list.remove('botbase.txt')
        for chat in chats_list:
            try:
                await bot.send_message(int(chat[:-4]), test_message, parse_mode='HTML')
            except:
                pass
