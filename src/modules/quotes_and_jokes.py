from bs4 import BeautifulSoup
from requests import get

# Config
jokes_address = 'https://www.generatormix.com/random-jokes'
quotes_address = 'http://bash.org/?random'

# Locals
i_just_joked_title = 'I joked:\n\n'
i_just_bashed_title = 'I bashed:\n\n'

@dp.message_handler(commands='stupidjoke')
async def get_joke(message):
    jokes_response = get(jokes_address)
    joke = BeautifulSoup(jokes_response.text, 'lxml').find('blockquote').text
    await message.reply(joke)
    await update_stats(message)
    if str(message.chat.id) not in logs_disabled_chats_list:
        await bot.send_message(config.logs_channel_id, i_just_joked_title + joke)

@dp.message_handler(commands='bash')
async def get_bash_quote(message: types.Message):
    quotes_response = get(quotes_address)
    quote = BeautifulSoup(quotes_response.text, 'lxml').find('p', class_='qt')
    await message.reply(quote.text)
    await update_stats(message)
    if str(message.chat.id) not in logs_disabled_chats_list:
        await bot.send_message(config.logs_channel_id, i_just_bashed_title + quote.text)
