from selenium import webdriver
import os, time

if not os.path.exists(f'{config.modules_dir}/webshots/'):
    os.mkdir(f'{config.modules_dir}/webshots')

options = webdriver.ChromeOptions()
options.add_argument("--headless")

driver = webdriver.Chrome(options=options)
driver.set_window_size(1024, 1024)

@dp.message_handler(commands='webshot')
async def webshot(message: types.Message):
    if message.text.startswith('https://'):
        await message.reply('Wait...')
        screenshot_name = f'{config.modules_dir}/webshots/{int(time.time())}.png'
        driver.get(message.text[9:])
        driver.get_screenshot_as_file(screenshot_name)
        await message.reply_document(open(screenshot_name, 'rb'))
