import aiohttp

@dp.message_handler(commands='wttr')
async def wttr_in(message: types.Message):
    if message.text[6:].isalpha():
        async with aiohttp.ClientSession() as session:
            async with session.get(f'https://wttr.in/{message.text[6:]}?m0&T') as wttr_resp:
                await message.reply(f'<code>{await wttr_resp.text()}</code>', parse_mode='HTML')

@dp.message_handler(commands='wfull')
async def wttr_in_full(message: types.Message):
    if message.text[7:].isalpha():
        wttr_media = types.MediaGroup()
        wttr_media.attach_photo(f'https://wttr.in/{message.text[7:]}.png')
        await message.reply_media_group(media=wttr_media)
