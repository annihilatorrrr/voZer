from requests import get
from pathlib import Path
from json import loads

@dp.message_handler(commands='comic')
async def random_comic(message: types.Message):
    comic_media = types.MediaGroup()
    comic_response = get('https://c.xkcd.com/random/comic')
    comic_page_url = f'{comic_response.url}'
    comic_response = loads(get(f'{comic_response.url}info.0.json').text)
    comic_url = comic_response['img']
    comic_title = comic_response['title']
    comic_media.attach_photo(comic_response['img'], comic_response['title'] + '\n' + comic_page_url)
    await message.reply_media_group(media=comic_media)
