# Imports
from gtts import gTTS

@dp.message_handler(commands='tts')
async def tts_handler(message: types.Message):
    randomint = random.randint(1000, 10000000)
    text_message = gTTS(message.text.replace('/tts', ''), lang='ru')
    text_message.save(f'Temp/voice_msg{randomint}.mp3')
    with open(f'Temp/voice_msg{randomint}.mp3', 'rb') as f:
        voice_message = f.read()
    await bot.send_voice(message.chat.id, voice_message)
    os.remove(f'Temp/voice_msg{randomint}.mp3')
