from random import randint

@dp.message_handler(commands='random')
async def generate_random_number(message: types.Message):
    args = message.text.split(' ')
    if len(args) != 3 or not args[1].isdigit() or not args[2].isdigit():
        await message.reply(wrong)
    else:
        await message.reply(randint(int(args[1]), int(args[2])))
